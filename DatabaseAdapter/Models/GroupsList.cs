﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAdapter.Models
{
    public class GroupsList
    {
        public int GroupsListId { set; get; }
        public int AdapterId { set; get; }
        public string GroupId { set; get; }
    }
}
