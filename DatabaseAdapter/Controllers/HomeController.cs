﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using DatabaseAdapter.Models;
namespace DatabaseAdapter.Controllers
{
    public class HomeController : Controller
    {
        Context context = new Context();   
        
        public ActionResult Index()
        {                    
            IEnumerable<Admin> admins = context.Admins;
            IEnumerable<Adapter> adapters = context.Adapters;
            IEnumerable<Student> students = context.Students;
            ViewBag.Admins = admins;
            ViewBag.adapters = adapters;
            ViewBag.students = students;
            return View();
        }
        [HttpGet]
        public ActionResult createAdmin()
        {
            
            return View();
        }
        [HttpPost]
        public void createadmin(Admin _admin)
        {
            _admin.Right = Models.User.Rights.ADMINISTRATOR;
            context.Admins.Add(_admin);
            context.SaveChanges();
        }
        [HttpGet]
        public ActionResult createAdapter()
        {

            return View();
        }
        [HttpPost]
        public void createAdapter(Adapter _adapter)
        {
            _adapter.Points = 0;
            _adapter.Money = 0;
            _adapter.Right = Models.User.Rights.ADAPTER;
            context.Adapters.Add(_adapter);
            context.SaveChanges();
        }
        [HttpGet]
        public ActionResult createStudent()
        {
            return View();
        }
        [HttpPost]
        public void createStudent(Student _student)
        {
            _student.Right = Models.User.Rights.STUDENT;
            context.Students.Add(_student); 
            context.SaveChanges();
        }
        [HttpGet]
        public ActionResult addGroup(int id)
        {
            ViewBag.AdapterId = id;
            return View();
        }
        [HttpPost]
        public void addGroup(GroupsList _grouplist)
        {
            context.GroupsLists.Add(_grouplist);
            ////context.Adapters.Where<> - обновить по номеру адаптера инфу о его группах
            context.SaveChanges();
        }
    }
}
