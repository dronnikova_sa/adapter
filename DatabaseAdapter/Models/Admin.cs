﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAdapter.Models
{
    public class Admin : User
    {
        public string Email { set; get; }
    }
}
