﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DatabaseAdapter.Models
{
    class ContextInitializer : DropCreateDatabaseAlways<Context>
    {
        protected override void Seed(Context db)
        {
            db.Admins.Add(new Admin { 
                Name = "Кирилл", 
                Surname = "Лукашевич", 
                Email = "lukkir@mail.ru", 
                Username = "lirik",
                Password = "12345", 
                Right = User.Rights.ADMINISTRATOR
            });
            db.Adapters.Add(new Adapter {
                Name = "Вася",
                Surname = "Васин",
                Username = "vasya",
                Password = "321",
                Money = 0,
                Points = 10,
                Right = User.Rights.ADAPTER
            });
            db.Students.Add(new Student
            {
                Name = "Петя",
                Surname = "Петин",
                Username = "petya",
                Password = "321",
                Right = User.Rights.STUDENT,
                Group = "A1000"
            });
            db.SaveChanges();
        }
    }

    class Context : DbContext
    {

        static Context()
        {
            Database.SetInitializer<Context>(new ContextInitializer());
        }

        public Context()
            : base("Context")
        { }
        public DbSet<User> Users { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Adapter> Adapters { get; set; }
        public DbSet<Good> Goods { get; set; }
        public DbSet<GoodsList> GoodsLists { get; set; }
        public DbSet<AwardsList> AwardsLists { get; set; }
        public DbSet<CommentsList> CommentsLists { get; set; }
        public DbSet<GroupsList> GroupsLists { set; get; }
    }
}
