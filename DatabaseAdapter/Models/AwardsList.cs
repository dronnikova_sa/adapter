﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAdapter.Models
{
    public class AwardsList
    {
        public int AwardsListId { set; get; }
        public int AdapterId { set; get; }
        public int AwardId { set; get; }
    }
}
