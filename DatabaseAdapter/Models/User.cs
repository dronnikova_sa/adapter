﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAdapter.Models
{
    public class User
    {
        public enum Rights : byte
        {
            STUDENT,
            ADAPTER,
            ADMINISTRATOR
        }

        public int UserId { set; get; }
        public string Username { set; get; }
        public string Password { set; get; }

        public Rights Right { set; get; }
        public string Name { set; get; }
        public string Surname { set; get; }
    }
}
