﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAdapter.Models
{
    public class GoodsList
    {
        public int GoodsListId { set; get; }
        public int AdapterId { set; get; }
        public int GoodId { set; get; }
    }
}
