﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAdapter.Models
{
    public class Award
    {
        int points;
        protected Award(int bonusPoints)
        {
            points = bonusPoints;
        }

        public int getBonusPoints()
        {
            return points;
        }
    }

    public class Star : Award
    {
        public Star() 
            : base(10)
        {

        }
    }

    public class Medal : Award
    {
        public Medal()
            : base(20)
        {

        }
    }
}
