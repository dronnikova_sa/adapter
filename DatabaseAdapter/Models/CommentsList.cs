﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAdapter.Models
{
    public class CommentsList
    {
        public int CommentsListId { set; get; }
        public int AdapterId { set; get; }
        public string Comment { set; get; }
    }
}
