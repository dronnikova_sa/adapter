﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAdapter.Models
{
    public class Good
    {
        public int GoodId { set; get; }
        public int Price { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }
        public bool OnStock { set; get; }
    }
}
