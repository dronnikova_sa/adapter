﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAdapter.Models
{
    public class Adapter: User
    {
        public int Points { set; get; }
        public int Money { set; get; }
        public IEnumerable<int> Groups { set; get; }
        public IEnumerable<int> Awards { set; get; }
    }
}
